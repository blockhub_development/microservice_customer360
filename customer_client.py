
from __future__ import print_function

import argparse

import grpc

import customer_pb2
import customer_pb2_grpc


def run(host, port):
    channel = grpc.insecure_channel('%s:%d' % (host, port))
    stub = customer_pb2_grpc.CustomerPredictorStub(channel)
    request = customer_pb2.CustomerPredictRequest(
        customer_age = 55,
        customer_gender = 0,
        customer_income = 1,
        customer_headOfhousehold = 0,
        customer_noOfhousehold = 2,
		customer_monthOfresidence = 50,
		customer_t1_30 = 332,
		customer_t2_30 = 13902,
		customer_t3_30 = 10963,
		customer_t1_10 = 133,
		customer_t2_10 = 4473,
		customer_t3_10 = 3586,
		customer_t1_3 = 26,
		customer_t2_3 = 1824,
		customer_t3_3 = 1403
    )
    response = stub.PredictCustomers(request)
	
    print("Probabilty of Customer Buy Product 1: " + str(response.buy_type2 * 100)+'%')
    print("Probabilty of Customer Buy Product 2: " + str(response.buy_type3 * 100)+'%')
    print("Probabilty of Customer Buy Product 3: " + str(response.buy_type4 * 100)+'%')
    print("Probabilty of Customer Buy None of Three Products: " + str(response.buy_type1 * 100)+'%')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', help='host name', default='localhost', type=str)
    parser.add_argument('--port', help='port number', default=50052, type=int)

    args = parser.parse_args()
    run(args.host, args.port)
