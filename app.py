from flask import Flask, Response, request
from flask_cors import CORS, cross_origin
import sys
import json

from google.protobuf.json_format import MessageToJson
from client_wrapper import ServiceClient

import customer_pb2_grpc
import customer_pb2

app = Flask(__name__)
CORS(app)
app.config['customers'] = ServiceClient(customer_pb2_grpc, 'CustomerPredictorStub', 'localhost', 50052)

@app.route('/customers/', methods=['POST'])
def customer_get():
    # {"age": 55, "gender": 0, "income": 1, "headOfhousehold": 0, "noOfhousehold": 2, "monthOfresidence": 50,
    #  "t1_30": 332, "t2_30": 23902, "t3_30": 10963, "t1_10": 133, "t2_10": 14473, "t3_10": 3586, "t1_3": 26,
    #  "t2_3": 6824, "t3_3": 1403}
    data = request.get_json()
    print(data['t3_3'])
    model_data = customer_pb2.CustomerPredictRequest(
        customer_age=int(data['age']),
        customer_gender=int(data['gender']),
        customer_income=int(data['income']),
        customer_headOfhousehold=int(data['headOfhousehold']),
        customer_noOfhousehold=int(data['noOfhousehold']),
        customer_monthOfresidence=int(data['monthOfresidence']),
        customer_t1_30=int(data['t1_30']),
        customer_t2_30=int(data['t2_30']),
        customer_t3_30=int(data['t3_30']),
        customer_t1_10=int(data['t1_10']),
        customer_t2_10=int(data['t2_10']),
        customer_t3_10=int(data['t3_10']),
        customer_t1_3=int(data['t1_3']),
        customer_t2_3=int(data['t2_3']),
        customer_t3_3=int(data['t3_3'])
    )

    def get_customer():
        response = app.config['customers'].PredictCustomers(model_data)
        print(response)
        yield MessageToJson(response)

    return Response(get_customer(), content_type='application/json')


def customers_get_test():

    request = customer_pb2.CustomerPredictRequest(
        customer_age = 55,
        customer_gender = 0,
        customer_income = 1,
        customer_headOfhousehold = 0,
        customer_noOfhousehold = 2,
        customer_monthOfresidence = 50,
        customer_t1_30 = 332,
        customer_t2_30 = 23902,
        customer_t3_30 = 10963,
        customer_t1_10 = 133,
        customer_t2_10 = 14473,
        customer_t3_10 = 3586,
        customer_t1_3 = 26,
        customer_t2_3 = 6824,
        customer_t3_3 = 1403
    )
    def get_customer():
        response = app.config['customers'].PredictCustomers(request)
        print(response)
        yield MessageToJson(response)
    return Response(get_customer(), content_type='application/json')

if __name__ == '__main__':
    # app.run()
    # app run
    app.run(debug=True, host='0.0.0.0')
