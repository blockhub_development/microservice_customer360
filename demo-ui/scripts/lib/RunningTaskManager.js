﻿

function RunningTaskManager(groupName, baseRTCUrl) {
    var self = this;
    self.active = false;
    self.inrun = false;
    self.inGroupRefresh = false;
    self.urlForGroup = baseRTCUrl + '/task/group';
    self.urlForTaskList = baseRTCUrl + '/task/getlist';
    self.tasks = new Array();
    self.taskRefreshTimer = null;
    self.groupUpdateTimer = null;
    
    //jQuery Timer-doesn't work
    //self.taskRefreshTimer = $.timer(function() { self.taskUpdate() }) ;
    //self.groupupdateTimer = $.timer(function() { self.groupUpdate() });
    
    
    self.setActive = function (active) {
        self.active = active;
        if (self.active)
            self.startGroupUpdateTimer();
        else
            self.stopGroupUpdateTimer();
    };

    self.startTaskRefreshTimer = function () {
        if (self.taskRefreshTimer == null) {
            console.log("***start task timer");
            self.taskRefreshTimer = window.setInterval(function () { self.taskUpdate() }, 4000);
        }
    };

    self.stopTaskRefreshTimer = function () {
        console.log("*****stop task timer");
        if (self.taskRefreshTimer != null) {
            window.clearInterval(self.taskRefreshTimer);
            self.taskRefreshTimer = null;
        }
        
        //self.taskRefreshTimer.stop();
    };

    self.startGroupUpdateTimer = function () {
        if (self.groupUpdateTimer == null) {
            console.log("*****start group timer");
            self.groupUpdateTimer = window.setInterval(function () { self.groupUpdate() }, 5000);
            //self.groupUpdateTimer.play();
        }
       
    };

    self.stopGroupUpdateTimer = function () {
        if (self.groupUpdateTimer != null) {
            console.log("*****stop group timer");
            window.clearInterval(self.groupUpdateTimer);
            self.groupUpdateTimer = null;
            //self.groupUpdateTimer.stop();
        }
    };

    self.taskUpdate = function () {
        if (self.inrun)
            return;
        else {
            self.inrun = true;
        }

        $.ajax({
            url: self.urlForTaskList,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            data: JSON.stringify(self.getTaskIds()),
            success: function (tasksFromServer) {
                self.inrun = false;

                console.log("start refresh task success ajax call " + self.inrun);
                if (tasksFromServer == null || tasksFromServer.length == 0) {
                    self.stopTaskRefreshTimer();
                } else {
                    self.addToRunningTaskList(tasksFromServer);
                    self.updateRunningTasks(tasksFromServer);
                }

                
            },
            error: function () {
                //TODO: need to handle the timer when error occurred
                console.log("Error on TaskUpdate");
            },
            complete: function (data) {
                console.log("task call completed");
                self.inrun = false;
            }

        });

        
    };
   
    self.groupUpdate = function () {
        if (groupName == null) {
            return;
        }
        console.log("group update call");
        if (self.inGroupRefresh)
            return;
        else {
            self.inGroupRefresh = true;
            console.log("group update starts");
            $.get(self.urlForGroup + "/" + groupName, function(runningtaskList) {
                //success
                console.log("call: " + self.urlForGroup + " data: " + runningtaskList);

                self.addToRunningTaskList(runningtaskList);
                //self.updateRunningTasks(runningtaskList);
            }).fail(function () {
                console.log("call: " + self.urlForGroup + " failed");
            }).done(function () {
                console.log("call: " + self.urlForGroup + " done");
                self.inGroupRefresh = false;
            });
        }
    };

    self.addToRunningTaskList = function (tasksfromServer) {
        var newRunningTasksThatIdidNotSeeBefore = new Array();
        if (tasksfromServer != null) {
            $.each(tasksfromServer, function (index, runningTask) {
                var found = false;
                $.each(self.tasks, function (index, localTask) {
                    if (localTask.id == runningTask.id) {
                        found = true;
                        return;
                    }
                });
               
                if (!found) {
                    self.tasks.push(runningTask);
                    newRunningTasksThatIdidNotSeeBefore.push(runningTask);
                }
            });
        }

        if (newRunningTasksThatIdidNotSeeBefore.length > 0) {
            //fire Event
            if (self.newTaskEvent != null) {
                self.newTaskEvent(newRunningTasksThatIdidNotSeeBefore);
            }
        }

        if (self.tasks.length > 0) {
            self.startTaskRefreshTimer();
        } else {
            self.stopTaskRefreshTimer();
        }

    };

    self.updateRunningTasks = function (tasksfromServer) {
        var removed = false;

        $.each(tasksfromServer, function (index, serverTask) {
            var localTask = null;
            $.each(self.tasks, function (index, task) {
                if (task.id == serverTask.id) {
                    localTask = task;
                    return;
                }
            });

            if (localTask != null) {
                //implements three events

                //new message event
                if (serverTask.messageList != null) {
                    if (serverTask.messageList.length > localTask.messageList.length) {
                        var newMessageList = serverTask.messageList.slice(localTask.messageList.length);
                        localTask.messageList = serverTask.messageList;
                        if (self.taskMessageReceivedEvent != null) {
                            self.taskMessageReceivedEvent(localTask, newMessageList);
                        }
                    }
                }
               
                //status change event
                if (serverTask.status != localTask.status) {
                    localTask.status = serverTask.status;
                    if (self.taskStatusChangeEvent != null) {
                        try {
                            self.taskStatusChangeEvent(localTask, localTask.status);
                        }
                        catch (err) {
                            console.log(err);
                        }
                    }
                    if (localTask.status == "FINISH" || localTask.status == "ERROR" || localTask.status == "CANCEL") {
                        //remove localTask from task list
                        self.tasks = $.grep(self.tasks, function (o, i) { return o.id == localTask.id }, true);
                        removed = true;
                    }

                    switch (localTask.status) {
                        case "FINISH":
                            if (self.taskStatusFinishEvent != null) {
                                try {
                                    self.taskStatusFinishEvent(localTask);
                                }
                                catch (err) {
                                }
                            }
                               
                            break;
                        case "ERROR":
                            if (self.taskStatusErrorEvent != null) {
                                try {
                                    self.taskStatusErrorEvent(localTask);
                                }
                                catch (err) {
                                }
                            }
                            break;
                        case "CANCEL":
                            if (self.taskStatusCancelEvent != null) {
                                try {
                                    self.taskStatusCancelEvent(localTask);
                                }
                                catch (err) {
                                }
                            }
                            break;
                    }
                    
                }

                //Meta Data Change Event
                for (var key in localTask.metaData) {
                    if (key in serverTask.metaData) {
                        var localValue = localTask.metaData[key];
                        var newValue = serverTask.metaData[key];
                        if (newValue != localValue) {
                            localTask.metaData[key] = newValue;
                            if (self.taskMetaDataChangeEvent != null) {
                                try {
                                    self.taskMetaDataChangeEvent(localTask, key, localValue, newValue);
                                }
                                catch (err) {
                                    console.log(err.message);
                                }
                            }
                        }
                    }
                }

            }
        });

        if (self.tasks.length == 0 && removed) {
            self.stopTaskRefreshTimer();
            if (self.allTaskCompleteEvent != null) {
                try {
                    self.allTaskCompleteEvent();
                }
                catch (err) {
                    console.log(err.message);
                }
            }
        }

    };

    self.run = function (runTaskUrl, httpVerb, inputData) {
        //runTaskUrl sample: "http://127.0.0.1:8080/upgrade/n5k/runprelim/1"
        return $.ajax({
            url: runTaskUrl,
            headers: {"groupname" : self.groupName},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: httpVerb,
            data: JSON.stringify(inputData),
            success: self.addToRunningTaskList,
            error: function (err) {
                debugger;
                console.log(err.message);
            }
        }).done(function () {
            
        });

    }

    self.getTaskIds = function () {
        var ids = new Array();
        $.each(self.tasks, function (i, task) {
            ids.push(task.id);
        });

        return ids;
    };

    self.newTaskEvent = function (newTaskList) {

    };

    self.taskStatusChangeEvent = function (task, newStatus) {

    };

    self.taskMessageReceivedEvent = function (task, newMessageList) {

    };

    self.taskMetaDataChangeEvent = function (task, key, oldVal, newVal) {

    };

    self.taskStatusErrorEvent = function (task) {

    };

    self.taskStatusFinishEvent = function (task) {

    };

    self.taskStatusCancelEvent = function (task) {

    };

    self.allTaskCompleteEvent =function() {
    };

}


function RunningTask() {
    var self = this;
    self.id=-1;
    self.uuid = "";
    self.status="";
    self.messageList = new Array();
    self.metaData = {};
    self.errorMessage = "";
}

function TaskStatusMessage() {
    var self = this;
    self.id=-1;
    self.timestamp=new Date();
    self.logType="INFO";
    self.message="";

}