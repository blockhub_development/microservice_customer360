consumer=angular
    .module('app.consumer', ['ui.router', 'app.data.consumer','ui.bootstrap'])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'consumer',
            url: '/consumer',
            templateUrl: 'scripts/src/templates/consumer.html',
            controller: "consumerController",
            resolve: {
            },
        });
    }])
    .controller("consumerController", ["$scope", "$state", "consumerDataService",function($scope, $state, consumerDataService) {
        $scope.model={};
        $scope.projectName = "Consumer";
        $scope.result={};
        //set default value
        $scope.model.t1_30=332;
        $scope.model.t2_30=23902;
        $scope.model.t3_30=10963;
        $scope.model.t1_10=133;
        $scope.model.t2_10=14473;
        $scope.model.t3_10=3586;
        $scope.model.t1_3=26;
        $scope.model.t2_3=6824;
        $scope.model.t3_3=1403;
        $scope.save = function(){
            console.log('age' + $scope.model.age)
            consumerDataService.save($scope.model).then(function(response) {
                $scope.result=response.data;
            }, function(data) {
                //error
                console.log('error');
            });

        }

    }]);