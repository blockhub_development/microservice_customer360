consumer=angular
    .module('app.edi', ['ui.router','ui.bootstrap'])
    .config(["$stateProvider", function($stateProvider) {
        $stateProvider.state({
            name: 'edi',
            url: '/edi',
            templateUrl: 'scripts/src/templates/edi.html',
            controller: "ediController",
            resolve: {
            },
        });
    }])
    .controller("ediController", ["$scope", "$state",function($scope, $state) {
        $scope.model={};
        $scope.projectName = "EDI";
    }]);