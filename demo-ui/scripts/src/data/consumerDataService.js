/**
 * Created by tyan2 on 6/10/2018.
 */
angular
    .module('app.data.consumer', ["app.config"])
    .factory('consumerDataService', ['$http', "apiBaseUrl", "$q", function($http, apiBaseUrl, $q){
        var factory = {};

        factory.save=function (_consumer) {
            req={"age": _consumer.age, "gender": _consumer.gender, "income": _consumer.income,
                "headOfhousehold": _consumer.headOfhousehold, "noOfhousehold": _consumer.noOfhousehold,
                "monthOfresidence": _consumer.monthOfresidence, "t1_30": _consumer.t1_30, "t2_30": _consumer.t2_30,
                "t3_30": _consumer.t3_30, "t1_10": _consumer.t1_10, "t2_10": _consumer.t2_10,
                "t3_10": _consumer.t3_10, "t1_3": _consumer.t1_3, "t2_3": _consumer.t2_3, "t3_3": _consumer.t3_3};

            return $http.post(apiBaseUrl + "/customers/", req);
        }


        function nonCachedGet(url) {
            return $http({
                method: 'GET',
                url: url,
                cache: false
            });
        }

        return factory;
    }]);