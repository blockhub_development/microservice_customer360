var Auth=function(appName, authUrl, debug) {
    debugger;
    self=this;
    if(arguments.length==1) {
        appName=arguments[0];
        authUrl="http://no2-dev.cisco.com/";
    }
    if(arguments.length==2) {
        appName=arguments[0];
        authUrl=arguments[1];
    }
    debug = (typeof debug !== 'undefined') ?  true : false;

    self.userUrl=authUrl + 'user';
    self.signOutUrl=authUrl+'signout';
    self.signOnUrl=authUrl.concat('verify/' + appName);
    self.appName=appName;
    //this.app=!localStorage.getObject(appName) ? sessionStorage.getObject(appName) : localStorage.getObject(appName);
    self.app=sessionStorage.getObject(appName);
    self.userId=!self.app ? null : self.app.userId;
    self.remaining= !localStorage.getObject(appName) ? false : true;

    self.signOut=function() {
        var _session=authUtil.getSession(self.appName);
        $.ajax({
            type: "GET",
            //headers: {'x-sso-appname': appName},
            url: self.signOutUrl+ '/' + _session,
            success: function(data){
                return true;
            },
            error: function (data) {
                return false;
            }
        });
    };

    self.init=function () {
        if(!this.userId) {
            var referUrl=document.referrer;
            var url=window.location.href;
            var lastPart = url.split("?").pop();
            var arr=lastPart.split('=');
            var _session='';
            if(arr.length==2 && arr[0]=='session' && arr[1]) {
                _session=arr[1];
            }

            $.ajax({
                type: "GET",
                //headers: {'x-sso-appname': appName},
                url: self.userUrl+ '/' + _session,
                async: false,
                success: function(data){
                    if(data=='anonymousUser') {
                        window.location = self.signOnUrl;
                    } else {
                        var obj={appName: appName, userId: data, session: _session};
                        sessionStorage.setObject(appName, obj);
                    }
                },

                error: function (data) {
                    window.location = self.signOnUrl;
                }
            });
        }
    };

    if(!debug)
        self.init();
};

var authUtil={
    getUser: function(appName) {
        return !sessionStorage.getObject(appName) ? 'default' : sessionStorage.getObject(appName).userId;
    },
    getSession: function(appName) {
        return !sessionStorage.getObject(appName) ? '' : sessionStorage.getObject(appName).session;
    }
};

Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function(key) {
    return JSON.parse(this.getItem(key));
};

Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function(key) {
    return JSON.parse(this.getItem(key));
};