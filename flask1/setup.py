import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name="flask",
    version="1.0.1",
    author="tommy",
    description="flask",
    packages=find_packages(),
    long_description=read('README'),
    install_requires=['flask']
)

